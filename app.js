const express = require ('express')
const app = express()
const port = 3000

const fs = require('fs')
const multer = require('multer')
const pdfMerge = require('easy-pdf-merge')
const PORT = process.env.PORT || 3000

app.use(express.json())
app.use(express.urlencoded({extended:false}));
app.use(express.static('public'))

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
    }
})

const dir = "public";
const subDirectory = "public/uploads";

if(!fs.existsSync(dir)){
    fs.mkdirSync(dir);
    fs.mkdirSync(subDirectory);
}


app.get('/mergepdf', (req, res) => {
    res.render('/mergepdf',{title:'Merge PDF files online'})
})

const mergepdffilter = function (req,file, cb){
    let ext = path.extname(file.originalname);

    if(ext !== ".pdf"){
         return cb("This file type is not supported");
    }
    cb(null,true);
}

const mergedPdfFilesUpload = multer({storage:storage, fileFilter:mergepdffilter});

app.post('/mergedpdf',mergedPdfFilesUpload.array('file', 100),(req,res) => {
    const files = [];
    const outputFilePath = Date.now() + "output.pdf";
    if(req.files){
        req.files.forEach(file => {
            console.log(file.path)
            files.push(file.path)
        }); 
    }

    pdfMerge(files,{output:outputFilePath})
    .then((buffer) =>{
        res.download(outputFilePath,(err) =>{
            if(err){
                fs.unlinkSync(outputFilePath)
                res.send("Some error occured while downloading the file")
            }
            fs.unlinkSync(outputFilePath)
        })
    }).catch((err) =>{
        if(err){
            fs.unlinkSync(outputFilePath)
            res.send("Some error occured while merging the pdf file")
        }
    })
}

)

app.listen(port, ()=>{
    console.log(`Example app listen at http://localhost:${port}`)
})

